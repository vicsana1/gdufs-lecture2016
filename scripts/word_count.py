from pyspark import SparkContext
from pyspark.streaming import StreamingContext

sc = SparkContext(appName="rt_echo")
ssc = StreamingContext(sc,1)

lines = ssc.socketTextStream("localhost",9999)

Words = lines.flatMap( lambda line : line.split(" ") )
WordCount = Words.map( lambda word: (word,1) ).reduceByKey( lambda x,y: x+y )
WordCount.pprint()

ssc.start()
ssc.awaitTermination()
