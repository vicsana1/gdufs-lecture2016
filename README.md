This project contains my introductory lecture to Big Data processing with Spark.

It contains the slides, the python notebook, and additional code.

This talk was given at Guandong University of Foreign Studies, April 2016, 
and it aims to introduce the student into the use of Apache Spark. Spark core
functionalities are reviewed, as well as Spark SQL, MLlib, Spark Streaming, and
Graphx